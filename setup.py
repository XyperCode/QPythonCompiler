from distutils.core import setup

setup(
    name='PyCompiler',
    version='1.0.0',
    packages=['PyCompiler'],
    url='https://github.com/Qboi123/PythonCompiler',
    license='',
    author='Qboi123',
    author_email='',
    description='Python compiler, compiles python to exe'
)
